import numpy as np
import cv2

cap = cv2.VideoCapture(0)

while(True):
    
    ret, frame = cap.read()
    
    if ret==True:
        
        gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        _,th=cv2.threshold(gray,100,255,cv2.THRESH_BINARY_INV)
    
        cnts,_=cv2.findContours(th,cv2.RETR_EXTERNAL,cv2.CHAIN_APPROX_SIMPLE)
    
        cv2.imshow('Original',gray)
        cv2.imshow('Binaria',cnts)
    
        if cv2.waitKey(1) & 0xFF == ord('q'):
            break


cap.release()
cv2.destroyAllWindows()
