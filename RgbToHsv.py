from tkinter import *
from PIL import Image, ImageTk #pip install pil

import cv2
import sys
import numpy as np

def onClossing():
    print("lower=np.array(["+str(hMin.get())+","+str(sMin.get())+","+str(vMin.get())+"])")
    print("upper=np.array(["+str(hMax.get())+","+str(sMax.get())+","+str(vMax.get())+"])")
    root.quit()         #finaliza este programa
    cap.release()
    print("Ip cam disconected")
    root.destroy()      #Cierra la ventana creada

def hsvValue(int):
    hMin.set(slider1.get())
    hMax.set(slider2.get())
    sMin.set(slider3.get())
    sMax.set(slider4.get())
    vMin.set(slider5.get())
    vMax.set(slider6.get())
     
    
def callback():
    
        cap.open(url) 
        ret, frame = cap.read()

        if ret:
            
            
            hsv = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV) #Convertirlo a espacio de color HSV
            #Se crea un array con las posiciones minimas y maximas capturadas de los sliders
            lower=np.array([hMin.get(),sMin.get(),vMin.get()])
            upper=np.array([hMax.get(),sMax.get(),vMax.get()])
 
            #Deteccion de colores 
            mask = cv2.inRange(hsv, lower, upper)
            
            img = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
            img = Image.fromarray(img)
            img.thumbnail((400,400)) #Redimensionar imagen
            tkimage = ImageTk.PhotoImage(img)
            label.configure(image = tkimage )
            label.image = tkimage

            mask = Image.fromarray(mask)
            mask.thumbnail((400,400))
            tkimage1 = ImageTk.PhotoImage(mask)
            label1.configure(image = tkimage1 )
            label1.image = tkimage1
        
            root.after(1,callback)
            
        else:
            onClossing()
            

            
########################### Ip Cam ###########################
            
url='es2.jpg'
cap = cv2.VideoCapture(url)

if cap.isOpened():
    print("Ip Cam initializatized")
else:
    sys.exit("Ip Cam disconnected")
    
############################## HMI design #################
root = Tk()
root.protocol("WM_DELETE_WINDOW",onClossing)
root.title("Vision Artificial") # titulo de la ventana

label=Label(root) #image = imagen camara opencv / relief = decoracion de borde
label.grid(row=1,padx=20,pady=20)

label1=Label(root)
label1.grid(row= 1,column=1,padx=20,pady=20)

hMin = IntVar()
hMax = IntVar()
sMin = IntVar()
sMax = IntVar()
vMin = IntVar()
vMax = IntVar()

slider1 = Scale(root,label = 'Hue Min', from_=0, to=255, orient=HORIZONTAL,command=hsvValue,length=400)   #Creamos un dial para recoger datos numericos
slider1.grid(row = 2)
slider2 = Scale(root,label = 'Hue Max', from_=0, to=255, orient=HORIZONTAL,command=hsvValue,length=400)   #Creamos un dial para recoger datos numericos
slider2.grid(row = 2,column=1)
slider3 = Scale(root,label = 'Saturation Min', from_=0, to=255, orient=HORIZONTAL,command=hsvValue,length=400)   #Creamos un dial para recoger datos numericos
slider3.grid(row = 3)
slider4 = Scale(root,label = 'Saturation Max', from_=0, to=255, orient=HORIZONTAL,command=hsvValue,length=400)   #Creamos un dial para recoger datos numericos
slider4.grid(row = 3,column=1)
slider5 = Scale(root,label = 'Value Min', from_=0, to=255, orient=HORIZONTAL,command=hsvValue,length=400)   #Creamos un dial para recoger datos numericos
slider5.grid(row = 4)
slider6 = Scale(root,label = 'Value Max', from_=0, to=255, orient=HORIZONTAL,command=hsvValue,length=400)   #Creamos un dial para recoger datos numericos
slider6.grid(row = 4,column=1)

slider2.set(255)
slider4.set(255)
slider6.set(255)

root.after(1,callback) #Es un método definido para todos los widgets tkinter.
root.mainloop()
